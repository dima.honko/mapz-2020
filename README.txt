Ітерпретатор
12) Розробити мову, яка б дозволяла розпізнавати інформацію у HTML сторінках. На
відміну від попереднього завдання пошуку тексту, можна використовувати поняття
тегів, атрибутів, можливих включень і т.д. Рекомендовано переглянути синтаксис

подібних інструментів – Rapise, Selenium перед власною реалізацією. Можна
скористатись компонентою WinForms WebBrowser для завантаження і отримання
HTML-контенту, або компонентою CefSharp.

Патерни:
16)Міста. Є два або більше конкуруючих міст. Біля міста є навколишня територія, на якій
знаходяться корисні ресурси. Місто може розширювати свої границі, будуючи різні
споруди. Коли, збільшуючись, населення починає обробляти навколишні землі, воно
може також використовувати навколишні ресурси (відомі або приховані). Гравець
може будувати споруди, які можуть прискорювати ріст населення (ферми), захисні
споруди, тощо.
