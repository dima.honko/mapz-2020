package sample;

import java.io.Reader;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Buffer {
    final static int END_OF_SOURCE_CODE = -1;

    private int maxIndex = 0;
    private char localBuf[];

    private int indexBuf = 0;

    private boolean isEndSourseCode = false;

    public Buffer( String source ) {
        if( source.length() < 2 ){
            throw new IllegalArgumentException( "capacity must be more than 1" );
        }
        localBuf=source.toCharArray();
        maxIndex=source.length()-1;
    }

    public char getChar() {
        if(indexBuf<=maxIndex) {
            return localBuf[indexBuf++];
        } else {
            return '\0';
        }
    }

    public int peekChar(){
        if( isBufferEnded() ){
            return END_OF_SOURCE_CODE;
        }

        return localBuf[ indexBuf ];
    }

    public int peekSecondChar(){
        if( isBufferEnded() ){
            return END_OF_SOURCE_CODE;
        }

        return localBuf[ indexBuf + 1 ];
    }
    public void skipSpaces () {
        if (localBuf[indexBuf] == ' ' || localBuf[indexBuf] == '\n') {
            Pattern pattern = Pattern.compile("\\s+", Pattern.CASE_INSENSITIVE);

            char[] temparr = new char[maxIndex - indexBuf];
            for (int i = 0; i < maxIndex - indexBuf; i++) {
                temparr[i] = localBuf[i + indexBuf];
            }
            String saver = String.valueOf(temparr);
//            Pattern pattern = Pattern.compile(saver);
            try {
                Matcher matcher = pattern.matcher(saver);
                matcher.find();
                int t1 = matcher.start();
                int t2 = matcher.end();
                indexBuf += t2 - t1;
                // check all occurance
//                while (matcher.find()) {
//                    System.out.print("Start index: " + matcher.start());
//                    System.out.print(" End index: " + matcher.end() + " ");
//                }
            } catch (IllegalStateException e) {
                System.out.println(e);
            }
        }
    }

    public boolean isBufferEnded( ) {
        return (indexBuf > maxIndex);
    }

}