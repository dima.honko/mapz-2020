package sample.evaluator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Controller;

public class Show extends Application {
    Controller controller = null;

    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader();
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        controller = loader.getController();
        primaryStage.setTitle("HtmlInfo");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

    public void view() {
        try {
            start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init(String path) {
        if (controller != null) {
            controller.initHTML(path);
        }
    }
}
