package sample.evaluator;

import javafx.scene.control.TextArea;
import sample.Node;
import sample.Token;
import sample.TokenType;
import sample.evaluator.table.FunctionInfo;
import sample.evaluator.table.TableFunctions;
import sample.evaluator.table.TableVariables;
import sample.evaluator.table.VariableInfo;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Evaluator {
    Show view=null;
    private Node tree;
    private TextArea output;

    public Evaluator(Node inputTree, TextArea output){
        tree = inputTree;
        this.output=output;
    }


    public void translateProgram() {

        TableFunctions tf = new TableFunctions( "none" );

        List<Node> functions = tree.getChildrens();
        for (Iterator<Node> it = functions.iterator(); it.hasNext(); ) {
            Node function = it.next();
            translateFunction( function, tf );
        }
    }

    private void translateFunction(Node functionNode, TableFunctions tf) {

        TableVariables tv = new TableVariables();
        String functionName = (String) functionNode.getTokenValue();


        FunctionInfo.FunctionReturnType returnType = FunctionInfo.FunctionReturnType.convertFromTokenType( functionNode.getFirstChildren().getFirstChildren().getTokenType() );
        Node parlistNode = functionNode.getChildren(1);
        Node bodyNode = functionNode.getChildren(2);

        ArrayList<VariableInfo.VariableType> paramsArr = new ArrayList<>();
        List<Node> params = parlistNode.getChildrens();
        if( ! params.get(0).match( TokenType.EMPTY ) ) {
            for (Iterator<Node> it = params.iterator(); it.hasNext(); ) {
                Node param = it.next();

                Node argType = param.getFirstChildren().getFirstChildren();

                VariableInfo arg = new VariableInfo(argType.getTokenType());
                arg.setInitialization();
                tv.add((String) param.getTokenValue(), arg );

                paramsArr.add( VariableInfo.VariableType.convertFromTokenType( argType.getTokenType() ) );
            }
        }

        FunctionInfo curFunction = new FunctionInfo( returnType, paramsArr );
        tf.add( functionName, curFunction);

        List<Node> commands = bodyNode.getChildrens();
        for (Iterator<Node> iterator = commands.iterator(); iterator.hasNext(); ) {
            Node command = iterator.next();
            translateCommand( command, tv, tf, functionName );
        }
    }

    private void translateCommand(Node commandNode, TableVariables tv, TableFunctions tf, String curFuncName) {
        if( commandNode.getTokenType() == TokenType.EMPTY ){
            return;
        }

        List<Node> operands = commandNode.getChildrens();

        String varName = null;
        VariableInfo.VariableType exprType = null;
        Node operand = operands.get(0);
        switch( operand.getTokenType() ){
            case TYPE:
                VariableInfo var = new VariableInfo(operand.getFirstChildren().getTokenType());
                tv.add( (String) operands.get(1).getFirstChildren().getTokenValue(), var );
                translateCommand(operands.get(1),tv,tf,curFuncName);
                break;
            case NAME:
                varName = (String) operand.getTokenValue();
                if( ! tv.isDeclared(varName) ){
                    throw new RuntimeException("Variable must be declared");
                } else{
                    tv.setInitialization( varName );
                }

                Node exprNode = commandNode.getChildrens().get(2);

                try {
                     tv.setValue((String)operand.getTokenValue(),translateExpr( exprNode, tv, tf ));
                } catch (IOException e) {
                    e.printStackTrace();
                }


                break;
            case PRINT:
                varName = (String) operands.get(1).getTokenValue();
                if(tv.isDeclared(varName)) {
                    Number temp=tv.getValue(varName);
                    output.setText(output.getText()+temp+"\n");
                }
                else {
                    output.setText(output.getText()+varName+"\n");
                }

                break;
            case IF:
                boolean cond=false;
                try {
                    cond=findCondition(operands.get(1), tv, tf);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(cond){
                    List<Node> commands = operands.get(2).getChildrens();
                    for (Iterator<Node> iterator = commands.iterator(); iterator.hasNext(); ) {
                        Node command = iterator.next();
                        translateCommand( command, tv, tf, "if" );
                    }
                }
                else {
                    List<Node> commands = operands.get(3).getChildrens();
                    for (Iterator<Node> iterator = commands.iterator(); iterator.hasNext(); ) {
                        Node command = iterator.next();
                        translateCommand(command, tv, tf, "if");
                    }
                }


                break;
            case WHILE:
                cond=false;
                // TODO: разобраться с типами в условии, пока без них
                try {
                    cond=findCondition(operands.get(1), tv, tf);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                while(cond) {
                    Node bodyWhileNode = commandNode.getChildren(2);
                    List<Node> commands = bodyWhileNode.getChildrens();
                    for (Iterator<Node> iterator = commands.iterator(); iterator.hasNext(); ) {
                        Node command = iterator.next();
                        translateCommand(command, tv, tf, curFuncName);
                    }
                    try {
                        cond=findCondition(operands.get(1), tv, tf);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case NEW:
                if((operands.get(1).getTokenValue()).equals("Show")){
                    view=new Show();
                    view.view();
                }
        }
    }

    private Number translateExpr( Node exprNode, TableVariables tv, TableFunctions tf ) throws IOException {
        int arg1=0,arg2=0;
        if(exprNode.getTokenType() == TokenType.PLUS ||exprNode.getTokenType() == TokenType.MINUS ||
                exprNode.getTokenType() == TokenType.DIVISION ||exprNode.getTokenType() == TokenType.MULTIPLICATION ) {
            arg1 =(int) translateExpr(exprNode.getChildren(0), tv, tf);
            arg2 =(int) translateExpr(exprNode.getChildren(1), tv, tf);
        }
        switch (exprNode.getValue().getTokenType()) {
            case PLUS:
                return arg1+arg2;
            case MINUS:
                return arg1-arg2;
            case MULTIPLICATION:
                return arg1*arg2;
            case DIVISION:
                return arg1/arg2;
            case NUMBER:
                return (Integer) exprNode.getValue().getTokenValue();
            case NAME:
                String varName = (String) exprNode.getTokenValue();
                return tv.getValue(varName);
            default:
                  throw new RuntimeException("TR: Imposible to parse");
        }
    }
    private boolean findCondition(Node condNode,TableVariables tv,TableFunctions tf) throws Exception{
        Node sign=condNode.getChildren(1);
        int arg1=0,arg2=0;
        try {
            arg1 = (int) translateExpr(condNode.getChildren(0), tv, tf);
            arg2 = (int) translateExpr(condNode.getChildren(2), tv, tf);
        } catch (Exception e){
            System.out.println(e);
        }
        switch ((String)sign.getTokenValue()){
              case "<":
              return arg1 < arg2;
              case "<=":
              return arg1 <= arg2;
              case "==":
              return arg1 == arg2;
              case "!=":
              return arg1 != arg2;
              case ">":
              return arg1 > arg2;
              case ">=":
              return arg1 >= arg2;
            default:
                throw new Exception("No sign in condition");
        }
    }
}
