package sample.evaluator.table;

import sample.TokenType;

public class VariableInfo {

    // для типа переменной
    public enum VariableType {
        INT, DOUBLE;

            public static VariableType convertFromTokenType(TokenType tokenType) {
                switch (tokenType) {
                    case INT:
                        return INT;
                    case DOUBLE:
                        return DOUBLE;
                    default:
                        throw new RuntimeException("Недопустимый тип");
                }
            }
        }

        private VariableType type;
    private boolean initialization = false;
        private Number value;


    public VariableInfo(TokenType type) {
        initialization = false;
        this.type = VariableType.convertFromTokenType(type);
    }

        public VariableInfo(TokenType type,Number value) {
            initialization = false;
            this.type= VariableType.convertFromTokenType(type);
            this.value=value;
        }

        // конструктор копирования
        public VariableInfo(VariableInfo parentVarInf) {
            this.initialization = parentVarInf.initialization;
            this.type = parentVarInf.type;
        }

        // проверка инициализации
        public boolean isInitialized() {
            return initialization;
        }

        // получить тип переменной
        public VariableType getType() {
            return type;
        }
        // установить флаг инициализации
        public void setInitialization() {
            initialization = true;
        }
        public Number getValue() {
            return value;
        }
        public void setValue(Number value) {
            this.value=value;
        }
}