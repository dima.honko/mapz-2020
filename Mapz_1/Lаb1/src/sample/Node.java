package sample;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private Token<?> value;
    private List< Node > listChild;

    public Token<?> getValue() {
        return value;
    }

    public TokenType getTokenType() {
        return value.getTokenType();
    }

    public Object getTokenValue() {
        return value.getTokenValue();
    }

    public Node( Token<?> newValue ) {
        value = newValue;
        listChild = new ArrayList< Node >();
    }


    public Node( TokenType typeValue ) {
        value = new Token<Object>( typeValue );
        listChild = new ArrayList< Node >();
    }

    public void setLeft( Node leftChild ){
        listChild.add(0, leftChild);
    }

    public void setRight( Node rightChild ){
        listChild.add( rightChild );
    }

    public List<Node> getChildrens(){
        return listChild;
    }


    public Node getFirstChildren(){
        if( listChild.size() > 0 ){
            return listChild.get(0);
        } else{
            // TODO:
            throw new RuntimeException( "Node have more than one children" );
        }
    }

    public Node getChildren( int index ){
        if( index < listChild.size() ){
            return listChild.get(index);
        } else{
            throw new RuntimeException( "NODE: Узел не имеет данного потомка" );
        }
    }

    public int getChildrenCount(){
        return listChild.size();
    }

    public boolean match( TokenType type ){
        return value.match( type );
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        Node other = (Node) obj;
        if (listChild == null) {
            if (other.listChild != null)
                return false;
        } else{
            if (!listChild.equals(other.listChild)){ //TODO
                return false;
            }
        }

        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value)) // TODO
            return false;

        return true;
    }

}
