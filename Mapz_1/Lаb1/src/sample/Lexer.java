package sample;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Lexer {
    private Buffer buffer;

    private Token<?> currentToken = null;

    private boolean isEndSourceCode = false;

    public Lexer( Buffer buffer ) {
        this.buffer = buffer;
    }

    public Token<?> peekToken() {
        if( currentToken == null ){
            makeToken();
        }

        return currentToken;
    }

    public Token<?> getToken(){
        if( currentToken == null ){
            makeToken();
        }
        Token<?> result = currentToken;
        makeToken();

        return result;
    }

    private void makeToken(){
        if( buffer.isBufferEnded() ){
            currentToken = new Token<String>( TokenType.END , "End" );
            return;
        }

        buffer.skipSpaces();


        char curChar = buffer.getChar();
        switch (curChar) {
            case '+':
                currentToken = new Token<Object>( TokenType.PLUS );
                break;
            case '-':
                currentToken = new Token<Object>( TokenType.MINUS );
                break;
            case '*':
                currentToken = new Token<Object>( TokenType.MULTIPLICATION );
                break;
            case '/':
                currentToken = new Token<Object>( TokenType.DIVISION );
                break;
            case '^':
                currentToken = new Token<Object>( TokenType.EXPONENTIATION );
                break;
            case '(':
                currentToken = new Token<Object>( TokenType.BRACKET_OPEN );
                break;
            case ')':
                currentToken = new Token<Object>( TokenType.BRACKET_CLOSE );
                break;
            case '{':
                currentToken = new Token<Object>( TokenType.BRACE_OPEN );
                break;
            case '}':
                currentToken = new Token<Object>( TokenType.BRACE_CLOSE );
                break;
            case ',':
                currentToken = new Token<Object>( TokenType.COMMA );
                break;
            case ';':
                currentToken = new Token<Object>( TokenType.SEMICOLON );
                break;

            default:
                if( Character.isDigit( curChar ) ){
                    currentToken = getNumberFromBuffer( curChar );
                } else{
                    if( Character.isAlphabetic( curChar ) || curChar == '_' ){
                        currentToken = getIdentificatorFromBuffer( curChar );
                    }else{
                        currentToken = getConditionSignFromBuffer( curChar );
                    }
                }

                break;
        }
    }

    private Token<?> getConditionSignFromBuffer(char curChar) {
        String sign = Character.toString(curChar);

        if (peekCharFromBuffer(0) == '=') {
            sign += buffer.getChar();
        }

        Token<?> result = null;
        if( sign.equals("=") ) {
            result = new Token<Object>(TokenType.ASSIGNMENT);
        }else if (sign.equals("<")) {
            result = new Token<String>(TokenType.SIGN, "<");
        } else if (sign.equals("<=")) {
            result = new Token<String>(TokenType.SIGN, "<=");
        } else if (sign.equals("==")) {
            result = new Token<String>(TokenType.SIGN, "==");
        } else if (sign.equals("!=")) {
            result = new Token<String>(TokenType.SIGN, "!=");
        } else if (sign.equals(">")) {
            result = new Token<String>(TokenType.SIGN, ">");
        } else if (sign.equals(">=")) {
            result = new Token<String>(TokenType.SIGN, ">=");
        } else{
            throw new RuntimeException("L: Unnown token");
        }

        return result;
    }


    private Token<?> getNumberFromBuffer( char curChar ) {
        int number = Character.getNumericValue( curChar );

        int shiftComma = -1;

        while( Character.isDigit( peekCharFromBuffer(0)) || (peekCharFromBuffer(0) == '.') ){

            if( (shiftComma > -1) || (peekCharFromBuffer(0) == '.') ){
                shiftComma++;
            }
            if( shiftComma != 0 ){
                number = 10 * number + Character.getNumericValue( buffer.getChar() );
            } else{
                buffer.getChar();
            }
        }

        switch (shiftComma) {
            case -1:
                return new Token<Integer>( TokenType.NUMBER, number );
            case 0:
                try {
                    throw new Exception("Number can`t and with dot");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return new Token<Integer>(TokenType.INT,1);
            default:
                double doubleNumber = number;
                double tenPower = Math.pow( 10, shiftComma );
                doubleNumber /= tenPower;
                return new Token<Double>( TokenType.NUMBER, doubleNumber );
        }
    }

    private Token<?> getIdentificatorFromBuffer( char curChar ) {

        String ident = Character.toString( curChar );

        while( Character.isAlphabetic( peekCharFromBuffer(0) ) ) {
            ident += buffer.getChar();
        }
        if( ! buffer.isBufferEnded() ){
        }else{
            char nextChar = (char) peekCharFromBuffer( 0 );
            switch (nextChar) {
                case ' ':

                case '+':
                case '-':
                case '*':
                case '/':
                case '^':

                case '(':
                case ')':
                case '{':
                case ',':
                case ';':
                case '=':


                case '<':
                case '>':
                case '!':
                    break;
                default:

                    break;
            }
        }

        Token<?> result = null;

        if( ident.equals( "return" ) ){
            result = new Token<Object>( TokenType.RETURN );
        } else if( ident.equals( "int" ) ){
            result = new Token<String>( TokenType.INT, "I" );
        } else if( ident.equals( "double" ) ){
            result = new Token<String>( TokenType.DOUBLE, "D" );
        } else if( ident.equals( "print" ) ){
            result = new Token<Object>( TokenType.PRINT );
        }else if( ident.equals( "void" ) ) {
            result = new Token<Object>( TokenType.VOID );
        } else if( ident.equals( "if" ) ) {
            result = new Token<Object>( TokenType.IF );
        } else if( ident.equals( "else" ) ) {
            result = new Token<Object>(TokenType.ELSE);
        } else if( ident.equals("while") ) {
            result = new Token<Object>(TokenType.WHILE);
        }else if( ident.equals("new") ) {
            result = new Token<Object>(TokenType.NEW);
        } else{
            result = new Token<String>( TokenType.NAME, ident );
        }

        return result;
    }


    private int peekCharFromBuffer( int serialIndex ){

        int ch = buffer.peekChar();

        if( ch == -1 ) {
            isEndSourceCode = true;
        }
            if( serialIndex == 1 ){
                ch = buffer.peekSecondChar();
                if( ch == -1 ){
                    isEndSourceCode = true;
                }
            }

        return ch;
    }
//    public boolean hasNextToken(){
//        if (buffer.isBufferEnded()) {
//            return false;
//        } else {
//            return true;
//        }
//    }
}