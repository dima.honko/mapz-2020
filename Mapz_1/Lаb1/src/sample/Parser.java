package sample;

public class Parser {
    private Lexer lexer;

    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }


    public Node parseProgram(){

        Node result = new Node( TokenType.PROGRAM );

        result.setLeft( parseFunction() );

        Node curFunction = parseFunction();
        while( ! curFunction.match( TokenType.EMPTY ) ){
            result.setRight( curFunction );
            curFunction = parseFunction();
        }

        return result;
    }

    public Node parseFunction(){

        Token<?> emptyToken = lexer.peekToken();

        if( emptyToken.match( TokenType.END ) ){
            return new Node( TokenType.EMPTY );
        }
        Node typeNode = parseType();
        Token<?> nameToken = lexer.getToken();
        if( ! nameToken.match( TokenType.NAME ) ){
            throw new RuntimeException("P: Nema of func undefined");
        }
        Token<?> openBracketToken = lexer.getToken();
        if( ! openBracketToken.match( TokenType.BRACKET_OPEN ) ){
            throw new RuntimeException("P: Open bracket absent");
        }
        Node parlistNode = parseParlist();
        Token<?> closeBracketToken = lexer.getToken();
        if( ! closeBracketToken.match( TokenType.BRACKET_CLOSE ) ){
            throw new RuntimeException("Close bracket absent");
        }

        Token<?> openBraceToken = lexer.getToken();
        if( ! openBraceToken.match( TokenType.BRACE_OPEN ) ){
            throw new RuntimeException("Open brace absent");
        }
        Node bodyNode = parseBody();
        Token<?> closeBraceToken = lexer.getToken();
        if( ! closeBraceToken.match( TokenType.BRACE_CLOSE ) ){
            // TODO: кинуть ошибку
            throw new RuntimeException("P: Close Brace absent");
        }

        Node result = new Node( new Token<String>( TokenType.FUNCTION, (String)nameToken.getTokenValue() ) );
        result.setLeft( typeNode );
        result.setRight( parlistNode );
        result.setRight( bodyNode );

        return result;
    }

    public Node parseParlist(){

        Token<?> closeBracketToken = lexer.peekToken();

        if( closeBracketToken.match( TokenType.BRACKET_CLOSE ) ){
            Node emptyParList = new Node( TokenType.PARAMS_LIST );
            emptyParList.setLeft( new Node( TokenType.EMPTY ) );
            return emptyParList;
        }

        Node result = new Node( TokenType.PARAMS_LIST );
        Token<?> commaToken;
        do{
            Node typeNode = parseType();
            Token<?> nameToken = lexer.getToken();
            if( ! nameToken.match( TokenType.NAME ) ){
                throw new RuntimeException("P: Name absent");
            }
            Node newParam = new Node( new Token<String>( TokenType.PARAM, (String) nameToken.getTokenValue() ) );
            newParam.setLeft( typeNode );
            result.setRight( newParam );
            commaToken = lexer.peekToken();
        } while( commaToken.match( TokenType.COMMA ) && commaToken == lexer.getToken() );

        if( ! (commaToken.match( TokenType.COMMA ) || commaToken.match( TokenType.BRACKET_CLOSE )) ){
            throw new RuntimeException("P: Uncorrect arguments");
        }

        return result;
    }

    public Node parseBody(){
        Node result = new Node( TokenType.BODY );
        Token<?> closeBraceToken = lexer.peekToken();
        if( closeBraceToken.match( TokenType.BRACE_CLOSE ) ){
            result.setLeft( new Node( TokenType.EMPTY ) );
            return result;
        }

        Node command = parseCommand();
        result.setLeft( command );

        do{
            if( !( command.getFirstChildren().getTokenType() == TokenType.IF ||
                    command.getFirstChildren().getTokenType() == TokenType.WHILE) ) {
                Token<?> semicolonToken = lexer.getToken();
                if (!semicolonToken.match(TokenType.SEMICOLON)) {
                    throw new RuntimeException("P: after command have to be \";\"");
                }
            }
            command = parseCommand();
            result.setRight( command );
        } while( ! command.getValue().match( TokenType.EMPTY ) );

        return result;
    }

    public Node parseCommand(){
        Node result = new Node( TokenType.COMMAND );
            Token<?> whatEver = lexer.peekToken();
            switch (whatEver.getTokenType()) {
                case BRACE_CLOSE:
                    result = new Node(TokenType.EMPTY);
                    break;
                case INT:
                case DOUBLE:
                    lexer.getToken();
                    Token<?> nameToken = lexer.peekToken();
                    if (nameToken.match(TokenType.NAME)) {
                        Node typeNode = new Node(TokenType.TYPE);
                        typeNode.setLeft(new Node(whatEver));
                        result.setLeft(typeNode);
                        result.setRight(parseCommand());
                    } else {
                        throw new RuntimeException("P: expected variable name");
                    }

                    break;
                case NAME:
                    lexer.getToken();
                    Token<?> assignToken = lexer.getToken();
                    if (assignToken.match(TokenType.ASSIGNMENT)) {
                        result.setLeft(new Node(whatEver));
                        result.setRight(new Node(assignToken));
                        result.setRight(parseExpr());
                    }
                    else {
                        throw new RuntimeException("P: No assigment to variable");
                    }
                    break;
                case PRINT:
                    lexer.getToken();
                    Token<?> openBracketToken = lexer.getToken();
                    if (!openBracketToken.match(TokenType.BRACKET_OPEN)) {
                        throw new RuntimeException("P: Open bracket absent");
                    }
                    Token<?> nameVariable = lexer.getToken();
                    if (!nameVariable.match(TokenType.NAME)) {
                        throw new RuntimeException("P: Wrong print parametr");
                    }
                    Token<?> closeBracketToken = lexer.getToken();
                    if (!closeBracketToken.match(TokenType.BRACKET_CLOSE)) {
                        throw new RuntimeException("P: Close bracket absent");
                    }
                    result.setLeft(new Node(whatEver));
                    result.setRight(new Node(nameVariable));

                    break;
            case IF:
                lexer.getToken();
                Token<?> openBracketIfToken = lexer.getToken();
                if( ! openBracketIfToken.match( TokenType.BRACKET_OPEN ) ){
                    throw new RuntimeException("P: expected \"(\" ater if");
                }

                result.setLeft( new Node(whatEver) );
                result.setRight( parseCondition() );

                Token<?> closeBracketIfToken = lexer.getToken();
                if( ! closeBracketIfToken.match( TokenType.BRACKET_CLOSE ) ){
                    throw new RuntimeException("P: expected \")\" ater condition");
                }

                Token<?> openBracketThenToken = lexer.getToken();
                if( ! openBracketThenToken.match( TokenType.BRACE_OPEN ) ){
                    throw new RuntimeException("P: expected \"{\" ater condition");
                }

                result.setRight( parseBody() );
                Token<?> closeBracketThenToken = lexer.getToken();
                if( ! closeBracketThenToken.match( TokenType.BRACE_CLOSE ) ){
                    throw new RuntimeException("P: expected \"}\" ater if");
                }

                Token<?> elseToken = lexer.peekToken();
                if( elseToken.match( TokenType.ELSE ) ){
                    lexer.getToken();
                    Token<?> openBraceElseToken = lexer.getToken();
                    if( ! openBraceElseToken.match( TokenType.BRACE_OPEN ) ){
                        throw new RuntimeException("P: expected \"{\" after else");
                    }
                    result.setRight( parseBody() );
                    Token<?> closeBraceElseToken = lexer.getToken();
                    if( ! closeBraceElseToken.match( TokenType.BRACE_CLOSE ) ){
                        throw new RuntimeException("P:expected \"}\" ");
                    }
                }
                break;
            case WHILE:
                lexer.getToken();
                Token<?> openBracketWhileToken = lexer.getToken();
                if( ! openBracketWhileToken.match( TokenType.BRACKET_OPEN ) ){
                    throw new RuntimeException("P: expected \"(\" ater while");
                }

                result.setLeft( new Node(whatEver) );
                result.setRight( parseCondition() );

                Token<?> closeBracketWhileToken = lexer.getToken();
                if( ! closeBracketWhileToken.match( TokenType.BRACKET_CLOSE ) ){
                    throw new RuntimeException("P:expected \")\" after condition");
                }

                Token<?> openBraceWhileToken = lexer.getToken();
                if( ! openBraceWhileToken.match( TokenType.BRACE_OPEN ) ){
                    throw new RuntimeException("P: expected \"{\" ");
                }

                result.setRight( parseBody() );

                Token<?> closeBraceWhileToken = lexer.getToken();
                if( ! closeBraceWhileToken.match( TokenType.BRACE_CLOSE ) ){
                    throw new RuntimeException("P: expected \"}\" after loop");
                }
                break;
                case NEW:
                    lexer.getToken();
                    Token<?> objName=lexer.getToken();
                    if(objName.match(TokenType.NAME)){
                        result.setRight(new Node(whatEver));
                        result.setRight(new Node(objName));
                    }
                    else {
                        throw new RuntimeException("P: Object supposed to have name");
                    }
                    break;
            default:
                throw new RuntimeException("P: Unknown command");
            }

        return result;
    }

    private Node parseCondition() {
        Node result = new Node( TokenType.CONDITION );

        result.setLeft( parseExpr() );
        Token<?> signToken = lexer.getToken();
        if( ! signToken.match( TokenType.SIGN ) ){
            throw new RuntimeException("P: Expected condition sign");
        }
        result.setRight( new Node(signToken) );
        result.setRight( parseExpr() );

        return result;
    }

    public Node parseArgList(){
        Token<?> closeBracketToken = lexer.peekToken();
        if( closeBracketToken.match( TokenType.BRACKET_CLOSE ) ){
            return new Node( TokenType.EMPTY );
        }
        Node result = new Node( TokenType.ARG_LIST );

        Token<?> commaToken;
        do{
            result.setRight( parseExpr() );
            commaToken = lexer.peekToken();
        } while( commaToken.match( TokenType.COMMA ) && commaToken == lexer.getToken() );

        if( ! commaToken.match( TokenType.BRACKET_CLOSE ) ){

            throw new RuntimeException("P: expected \")\"");
        }

        return result;
    }

    public Node parseType(){
        Node result = null;

        Token<?> typeToken = lexer.getToken();
        if( typeToken.match( TokenType.INT ) ||
                typeToken.match( TokenType.DOUBLE ) ||
                typeToken.match( TokenType.VOID )){

            Node specificType = new Node( typeToken );
            result = new Node( TokenType.TYPE );
            result.setLeft( specificType );
        }else{
            throw new RuntimeException("P: Incorrect Type");
        }

        return result;
    }


    public Node parseExpr(){
        Node result = parseTerm();

        Token<?> curToken = lexer.peekToken();
        while( curToken.match( TokenType.PLUS ) ||
                curToken.match( TokenType.MINUS ) ){

            // пропускаем знак
            lexer.getToken();

            // комплектуем дерево
            Node sign = new Node( curToken );
            sign.setLeft( result );
            sign.setRight( parseTerm() );

            result = sign;

            curToken = lexer.peekToken();
        }

        return result;
    }

    public Node parseTerm(){
        Node result =parseFactor();

        Token<?> curToken = lexer.peekToken();
        while( curToken.match( TokenType.MULTIPLICATION ) ||
                curToken.match( TokenType.DIVISION ) ){

            lexer.getToken();

            Node sign = new Node( curToken );
            sign.setLeft( result );
            sign.setRight( parseFactor() );

            result = sign;

            curToken = lexer.peekToken();
        }

        return result;
    }

    public Node parseFactor(){
        Node result = parsePower();

        Token<?> curToken = lexer.peekToken();
        if( curToken.match( TokenType.EXPONENTIATION ) ){
            lexer.getToken();

            Node exp = new Node( curToken );
            exp.setLeft( result );
            exp.setRight( parseFactor() );

            result = exp;
        }

        return result;
    }

    public Node parsePower(){
        Token<?> curToken = lexer.peekToken();
        if( curToken.match( TokenType.MINUS ) ){
            lexer.getToken();
            Node minus = new Node( curToken );
            minus.setLeft( parseAtom() );
            return minus;
        }

        return parseAtom();
    }

    public Node parseAtom(){
        Node result = null;

        Token<?> token = lexer.getToken();
        switch( token.getTokenType() ){
            case BRACKET_OPEN:
                result = parseExpr();
                token = lexer.getToken();
                if( token.match( TokenType.BRACKET_CLOSE ) ){
                    return result;
                }else{

                    throw new RuntimeException("P: expected \")\"");
                }
            case NUMBER:
                result = new Node( token );
                break;
            case NAME:
                Token<?> nextToken = lexer.peekToken();
                if( nextToken.match( TokenType.BRACKET_OPEN ) ){
                    // значит это вызов функции
                    lexer.getToken();
                    result = new Node( TokenType.CALL_FUNCTION );
                    result.setLeft( new Node( token ) );
                    result.setRight( parseArgList() );

                    nextToken = lexer.getToken();
                    if( ! nextToken.match( TokenType.BRACKET_CLOSE ) ){
                        throw new RuntimeException("P: expected \")\"");
                    }
                } else{
                    result = new Node( token );
                }

                break;
            default:
                throw new RuntimeException("P: error");
        }

        return result;
    }
}