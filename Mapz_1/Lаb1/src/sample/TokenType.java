package sample;

public enum TokenType {

    NUMBER,
    PLUS, MINUS,
    MULTIPLICATION, DIVISION,
    EXPONENTIATION,
    BRACKET_OPEN, BRACKET_CLOSE,


    EMPTY,

    PROGRAM,
    FUNCTION,
    NAME,
    BRACE_OPEN, BRACE_CLOSE,
    PARAMS_LIST,
    PARAM,
    COMMA,
    BODY,
    SEMICOLON,
    COMMAND,
    ARG_LIST,

    TYPE,
    INT, DOUBLE, VOID,

    ASSIGNMENT,
    CONDITION,
    SIGN,

    RETURN,

    NEW,

    INIT,
    CALL_FUNCTION,
    PRINT,

    IF, ELSE,
    WHILE,

    END,
}