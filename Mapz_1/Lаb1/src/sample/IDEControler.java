package sample;

import javafx.fxml.FXML;

import javafx.scene.control.TextArea;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import sample.evaluator.Evaluator;

import java.util.ArrayList;
import java.util.List;

public class IDEControler {
    private Node parseTree;
    @FXML
    TextArea input,output;
    @FXML
    TreeView<String> tree;
    ArrayList<Token> tokens=new ArrayList<>();
    @FXML
    public void run() {
        String source=input.getText();
        Buffer buffer=new Buffer(source);
        Lexer lex=new Lexer(buffer);
        Parser pars=new Parser(lex);
        parseTree=pars.parseProgram();
        Evaluator ev=new Evaluator(parseTree,output);
        ev.translateProgram();
        tree.setRoot(buildConTree(parseTree));

    }
    private TreeItem<String> buildConTree (Node parseTree){
        TreeItem<String> root;
        List<Node> childs=parseTree.getChildrens();
        if(parseTree.getTokenValue()==null) {
             root = new TreeItem<>(" " + parseTree.getTokenType());
        } else {
            root = new TreeItem<>( parseTree.getTokenType()+" -> "+parseTree.getTokenValue());
        }
        root.setExpanded(true);
        for (int i = 0; i <parseTree.getChildrenCount() ; i++) {
           root.getChildren().add(buildConTree(childs.get(i)));
        }
        return root;
    }
}
//    int main () {
//    int some = 2;
//    int an = 3;
//   if(an == 3){
//    int res = some + an ;
// }
// print (some);
//new Show;
//    }