package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Controller {
    @FXML
    TextArea input,output;
    String  html=null;
    Document doc=null;
    @FXML
    public void open() {
        FileChooser fileChooser = new FileChooser();
        File filePath = fileChooser.showOpenDialog(new Stage());
        fileChooser.setTitle("Open Resource File");
        if (filePath != null) {
            RandomAccessFile file = null;
            try {
                file = new RandomAccessFile(filePath, "r");
            } catch (FileNotFoundException var12) {
                var12.printStackTrace();
            }
                try {
                    if (file.length() == 0L) {
                        Alert message = new Alert(Alert.AlertType.INFORMATION);
                        message.setTitle("Довідка");
                        message.setHeaderText((String) null);
                        message.setContentText("Файл порожній");
                        message.show();
                    }

                    if(filePath.getName().matches(".*html.*")) {
                        String temp;
                        while ( (temp = file.readLine())!= null) {
                            html += temp + "\n";
                        }
                        outHTML();
                    }
                    else {
                        Alert message = new Alert(Alert.AlertType.ERROR);
                        message.setTitle("Помилка");
                        message.setHeaderText((String) null);
                        message.setContentText("Файл не .html");
                        message.show();
                        filePath=null;
                    }
                } catch (IOException var13) {
                    var13.printStackTrace();
                } catch (NoSuchElementException var15) {
                }
        } else {
            Alert message = new Alert(Alert.AlertType.INFORMATION);
            message.setTitle("Довідка");
            message.setHeaderText((String) null);
            message.setContentText("Зчитування інформації з нового файлу відмінено");
            message.show();
        }
    }
    @FXML
    public void clean(){
        output.clear();
        input.clear();
    }
    public void initHTML(String pathName){
        try {
            RandomAccessFile file=new RandomAccessFile(pathName,"r");
            String temp=null;
            html=null;
            while ((temp=file.readLine()) != null) {
                html+=temp + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        outHTML();
    }
    private void outHTML(){
        input.setText(html);
        parseHTML();
    }
    private void parseHTML(){
        doc = Jsoup.parse(html);
        getLinkInfo();
    }
    public void getLinkInfo(){
        Element link = doc.select("a").first();
        String linkHref = link.attr("href");
        String linkOuterH = link.outerHtml();
        String linkInnerH = link.html();
        output.setText(output.getText() + "\n" + "Info about reference:" + "\n" + linkHref + "\n" +
                linkOuterH + "\n"+linkInnerH+"\n");
    }
    public void parseBody () {

    }

}

