package creational;

public class MapLazy {
    private static volatile MapLazy instance;

    public static  MapLazy getInstance() {
        if (instance == null) {
            instance = new MapLazy();
        }
       return instance;
    }
    private MapLazy(){
        System.out.println("\nLazy class created\n");
    }
    public void mapConfig() {
        System.out.println("Do config");
    }
}

