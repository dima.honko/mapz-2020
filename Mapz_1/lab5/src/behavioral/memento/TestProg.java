package behavioral.memento;

import creational.prototype.Building;

import java.util.ArrayList;

public class TestProg {
    public static void main(String[] args) {
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player(null, "Player1" ));
        players.add(new Player(null, "Player2" ));
        players.add(new Player(null, "Player3" ));
        GameSettings game = new GameSettings("Game1", players);
        Menu menu = new Menu(game);
        game.print();
        menu.backup();
        game.addPlayer(new Player(null, "Player4"));
        menu.backup();
        game.addPlayer(new Player(null, "Player5"));
        menu.backup();
        game.removePlayer(0);
        menu.backup();
        System.out.println("Last version");
        game.print();
        menu.load(2);
        System.out.println("Load 2");
        game.print();
        menu.load(1);
        System.out.println("Load 1");
        game.print();
    }
}
