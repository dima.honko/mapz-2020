package behavioral.memento;

import java.util.ArrayList;

public class Menu {
    private ArrayList<Save> saves = new ArrayList<>();
    private GameSettings game = null;

    public Menu(GameSettings game) {
        this.game = game;
    }

    public void backup() {
        this.saves.add(this.game.save());
    }

    public void load(int index) {
        if (index >= 0 && index < saves.size()) {
            Save temp = saves.get(index);
            saves.remove(index);
            game.load(temp);
        }
    }
}
