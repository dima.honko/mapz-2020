package behavioral.memento;

import structure.flyweight.Map;

import java.util.ArrayList;

public class GameSettings {
    private String name;
    private ArrayList<Player> players;

    public GameSettings(String name, ArrayList<Player> players) {
        this.name = name;
        this.players = players;
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }

    public void addPlayer (Player player) {
        players.add(player);
    }

    public void removePlayer( int index)  {
        if (index >= 0 && index < players.size()) {
            players.remove(index);
        }
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Save save() {
        return new SaveGame(players, name);
    }
    public void load(Save save) {
        this.name = save.getName();
        this.players = new ArrayList<Player>();
        ArrayList<Player> players = save.getPlayers();
        for (Player player : players) {
            this.players.add(player);
        }
    }
    public void print ( ) {
        for ( Player player : players) {
            System.out.println(player.getName());
        }
    }
}
