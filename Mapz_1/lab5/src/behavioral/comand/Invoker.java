package behavioral.comand;

import java.util.ArrayList;

public class Invoker {
    private ArrayList<Command> commands = new ArrayList<>();
    private GameLogic reciever= null;

    public void init() {
        commands.add(new Float());
        commands.add(new Hiking());
        commands.add(new Walk());
    }

    public void add (Command command) {
        commands.add(command);
    }

    public void doSomthing (int index) {
        commands.get(index).execute();
    }
}
