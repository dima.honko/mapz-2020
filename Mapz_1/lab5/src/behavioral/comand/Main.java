package behavioral.comand;

public class Main {
    public static void main(String[] args) {
        Invoker invoker = new Invoker();
        GameLogic gameLogic = new GameLogic();
        invoker.init();
        invoker.add(new Complex("піхота","болоті",gameLogic));
        for (int i = 0; i < 4;i++) {
            invoker.doSomthing(i);
        }
    }
}
