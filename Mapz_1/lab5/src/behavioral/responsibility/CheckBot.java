package behavioral.responsibility;

public class CheckBot extends BaseCheck {
    private int requestPerMinute;
    private int request;
    private long currentTime;

    public CheckBot(int requestPerMinute) {
        this.requestPerMinute = requestPerMinute;
        this.currentTime = System.currentTimeMillis();
    }

    public boolean check(String email, String password) {
        if (System.currentTimeMillis() > currentTime + 60000) {
            request = 0;
            currentTime = System.currentTimeMillis();
        }

        request++;

        if (request > requestPerMinute) {
            System.out.println("Request limit exceeded!");
        }
        return checkNext(email, password);
    }
}
