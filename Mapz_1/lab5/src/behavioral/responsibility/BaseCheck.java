package behavioral.responsibility;

public abstract class BaseCheck {
    private BaseCheck next;

    public BaseCheck linkWith(BaseCheck next) {
        this.next = next;
        return next;
    }

    public abstract boolean check(String email, String password);


    protected boolean checkNext(String email, String password) {
        if (next == null) {
            return true;
        }
        return next.check(email, password);
    }
}
