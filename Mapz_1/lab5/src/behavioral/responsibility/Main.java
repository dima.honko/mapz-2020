package behavioral.responsibility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static Server server;

    private static void init() {
        server = new Server();
        server.register("bought", "1111");
        server.register("demo", "1111");
        server.register("premium", "1111");

        BaseCheck baseCheck = new CheckBot(2);
        baseCheck.linkWith(new CheckExistence(server))
                .linkWith(new CheckRole());

        server.setBaseCheck(baseCheck);
    }

    public static void main(String[] args) throws IOException {
        init();
        boolean success;
        do {
            System.out.print("Enter email: ");
            String email = reader.readLine();
            System.out.print("Input password: ");
            String password = reader.readLine();
            success = server.logIn(email, password);
        } while (!success);
    }
}
