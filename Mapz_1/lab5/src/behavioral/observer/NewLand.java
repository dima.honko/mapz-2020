package behavioral.observer;

public class NewLand implements EventListener{

    @Override
    public void update(String name) {
        System.out.println("Нові землі на горизонті" + name);
    }
}
