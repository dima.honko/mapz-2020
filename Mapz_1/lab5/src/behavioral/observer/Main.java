package behavioral.observer;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<EventListener> heirs = new ArrayList<>();
        ArrayList<EventListener> lands = new ArrayList<>();
        ArrayList<EventListener> tech = new ArrayList<>();
        Notification notheir = new Notification();
        Notification notland = new Notification();
        Notification nottech = new Notification();
        for (int i = 0; i < 3; i++) {
            tech.add(new NewTech());
            heirs.add(new NewHeir());
            lands.add(new NewLand());
        }
        for (int i = 0; i < 3; i++) {
            nottech.subscribe(tech.get(i));
            notland.subscribe(lands.get(i));
            notheir.subscribe(heirs.get(i));
        }
        notheir.notify(" Василь");
        nottech.notify(" Компухтер");
        notland.notify(" Львів");
    }
}
