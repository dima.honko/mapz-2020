package behavioral.observer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Notification {
   ArrayList<EventListener> listeners = new ArrayList<>();

    public void subscribe(EventListener listener) {
        listeners.add(listener);
    }

    public void unsubscribe( EventListener listener) {
        listeners.remove(listener);
    }

    public void notify(String  name) {
        for (EventListener listener : listeners) {
            listener.update(name);
        }
    }
}
