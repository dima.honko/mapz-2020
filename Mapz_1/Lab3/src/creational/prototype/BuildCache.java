package creational.prototype;

import java.util.HashMap;
import java.util.Hashtable;

public class BuildCache {
    private static HashMap <String , Building > buildings = new HashMap<>();

    public static Building getBuilding(String id) {
        Building cachedBuilding = buildings.get(id);
        return (Building) cachedBuilding.clone();
    }

    public static void LoadBuildings() {
        Mine mine = new Mine();
        mine.setId("mine");
        buildings.put(mine.getId() , mine);

        Shop shop = new Shop();
        shop.setId("shop");
        buildings.put(shop.getId(),shop);

        Wall wall = new Wall();
        wall.setId("wall");
        buildings.put(wall.getId(),wall);

    }

}
