package structure.decorator;

public class Main {
    public static void main(String[] args) {
        Wall wall = new DecoratorCannon(new DecoratorSpear(new ConcreteWall()));
        wall.info();
        System.out.println();
        Wall wall1 = new DecoratorCannon(new DecoratorSpear(new DecoratorResin(new ConcreteWall())));
        wall1.info();
    }
}
