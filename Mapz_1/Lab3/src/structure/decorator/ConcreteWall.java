package structure.decorator;

public class ConcreteWall implements Wall {
    @Override
    public void info() {
        System.out.printf("Wall");
    }
}
