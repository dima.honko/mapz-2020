package structure.flyweight;

public class BuildingType {
    private String name;
    private String color;
    private String otherInfo;

    public BuildingType(String name, String color, String otherInfo) {
        this.name = name;
        this.color = color;
        this.otherInfo = otherInfo;
    }
    public void info() {
        System.out.println(name + " " + color + " " + otherInfo);
    }
}
