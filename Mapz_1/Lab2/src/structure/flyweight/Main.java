package structure.flyweight;

public class Main {
    public static void main(String[] args) {
        Map map = new Map();
        for (int i = 0;i < 5;i++) {
            map.build(random(0,1000), random(0,1000), "Shop","Blue" ,"groceries store" );
            map.build(random(0,1000), random(0,1000), "House","White" ,"regular house" );
        }
        map.showInfo();
    }
    private static int random(int min, int max) {
        return min + (int) (Math.random() * ((max - min) + 1));
    }
}
