package creational.abstractfactory;

public class Main {
    public static void main(String[] args) {

        MilitaryUnit infanitry = new MilitaryUnit(new FactoryInfanitry());
        MilitaryUnit archer = new MilitaryUnit(new FactoryArcher());
        MilitaryUnit navy = new MilitaryUnit(new FactoryNavy());
        MilitaryUnit cavalry = new MilitaryUnit(new FactoryCavalry());

        for (int i = 0; i < 10; i++) {
            infanitry.strike();
            archer.strike();
            navy.strike();
            cavalry.strike();
        }
        infanitry.move();
        archer.move();
        navy.move();
        cavalry.move();
    }
}
