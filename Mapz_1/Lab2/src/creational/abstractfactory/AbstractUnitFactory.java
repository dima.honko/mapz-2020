package creational.abstractfactory;


import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;


class MilitaryUnit {

    private Weapon weapon;
    private Movement movement;

    public MilitaryUnit(FactoryMilitaryUnit factory) {
        weapon = factory.createWeapon();
        movement = factory.createMovement();
    }

    public void move() {
        movement.info();
    }
    public void strike () {
        weapon.useWeapon();
    }
}

abstract class Weapon {
    protected int probability=50;
    abstract public void useWeapon ();
    protected String isHited() {
         int temp = (int) (Math.round(Math.random() * 100) + 1);
         if ((probability / temp) >= 1) {
             return "Попав";
         } else {
             return "Промах";
         }
     }
}
 class Bow extends Weapon {
     public Bow () {
         probability = 40;
     }
     @Override
     public void useWeapon() {
         System.out.println("Вистріл з лука:" + isHited());
     }

 }
 class Sword extends Weapon {
    public  Sword() {
        probability = 80;
    }
     @Override
     public void useWeapon() {
         System.out.println("Удар мечем:" + isHited());
     }

 }
 class Cannon extends Weapon {
     @Override
     public void useWeapon() {
         System.out.println("Вистріл з гармати:" + isHited());
     }
 }
 abstract class Movement {
    abstract public void info();
 }
 class Float extends Movement {
     @Override
     public void info() {
         System.out.println("Даний юніт пересувається лише по воді");
     }
 }
 class Walk extends Movement {
     @Override
     public void info() {
         System.out.println("Даний юніт пересувається лише по рівнині");
     }
 }
 class Hiking extends Movement {
     @Override
     public void info() {
         System.out.println("Може пересуватися як по горах так і по рівнинах");
     }
 }

abstract class FactoryMilitaryUnit {
    abstract public Weapon createWeapon();
    abstract public Movement createMovement();
}

class FactoryInfanitry extends FactoryMilitaryUnit {
    @Override
    public Weapon createWeapon() {
        return new Sword();
    }

    @Override
    public Movement createMovement() {
        return new Hiking();
    }
}
class FactoryCavalry extends FactoryMilitaryUnit {
    @Override
    public Weapon createWeapon() {
        return new Sword();
    }

    @Override
    public Movement createMovement() {
        return new Walk();
    }
}
class FactoryArcher extends FactoryMilitaryUnit {
    @Override
    public Weapon createWeapon() {
        return new Bow();
    }

    @Override
    public Movement createMovement() {
        return new Hiking();
    }
}
class FactoryNavy extends FactoryMilitaryUnit {
    @Override
    public Weapon createWeapon() {
        return new Cannon();
    }

    @Override
    public Movement createMovement() {
        return new Float();
    }
}