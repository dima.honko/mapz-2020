package creational;

public class Map {
    private static volatile Map instance;

    public static Map getInstance() {
        Map localInstance = instance;
        if (localInstance == null) {
            synchronized (Map.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new Map();
                }
            }
        }
        return localInstance;
    }
    private Map(){
        System.out.println("\nTread safe class created\n");
    }
    public void mapConfig() {
        System.out.println("Do config");
    }
}
class Main {
    public static void main(String[] args) {
//        for (int i = 0;i < 10; i++) {
//            MapLazy.getInstance();
//        }
        for (int i = 0;i < 10; i++) {
            GameThread t = new GameThread("Theread " + (i + 1));
            t.start();
        }
    }
}
class GameThread implements Runnable {
    private Thread t;
    private String threadName;

    GameThread(String name) {
        threadName = name;
        System.out.println("Creating " +  threadName );
    }

    public void run() {
        System.out.println("Running " +  threadName );
        Map.getInstance();
        MapLazy.getInstance();
    }

    public void start () {
        if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
        }
    }
}
