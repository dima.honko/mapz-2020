package creational.prototype;

public class Main {
    public static void main(String[] args) {
        BuildCache.LoadBuildings();

        Building mine=BuildCache.getBuilding("mine");
        Building mine2=BuildCache.getBuilding("mine");
        Building wall=BuildCache.getBuilding("wall");
        Building shop=BuildCache.getBuilding("shop");
        mine.getInfo();
        mine2.getInfo();
        wall.getInfo();
        shop.getInfo();
        if ( mine != mine2 ) {
            System.out.println("Об'єкти різні");
        }
    }
}
