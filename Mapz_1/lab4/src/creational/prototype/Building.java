package creational.prototype;

public abstract class Building implements Cloneable{
    private String id;
    protected int strenght;
    protected String wallType;
    protected int income;

    abstract void getInfo();

    public Object clone() {
        Object clone = null;

        try {
            clone = super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return clone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
class Mine extends Building {
    public Mine(){
        income = 30;
        strenght = 100;
        wallType = "wood";
    }
    @Override
    void getInfo() {
        System.out.println("Шахта");
    }
}
class Wall extends Building {
    public Wall(){
        income = -10;
        strenght = 1000;
        wallType = "stone";
    }
    @Override
    void getInfo() {
        System.out.println("Стіна");
    }
}
class Shop extends Building {
    public Shop(){
        income = 10;
        strenght = 200;
        wallType = "wood";
    }
    @Override
    void getInfo() {
        System.out.println("Магазин");
    }
}
