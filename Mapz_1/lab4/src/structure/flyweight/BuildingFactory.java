package structure.flyweight;

import java.util.HashMap;
import java.util.Map;

public class BuildingFactory {
    static Map<String, BuildingType> buildingTypes = new HashMap<>();

    public static BuildingType getBuildingType(String name, String color, String otherInfo) {
        BuildingType result = buildingTypes.get(name);
        if (result == null) {
            result = new BuildingType(name, color, otherInfo);
            buildingTypes.put(name, result);
        }
        return result;
    }
}
