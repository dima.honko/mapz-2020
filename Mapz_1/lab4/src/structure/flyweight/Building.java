package structure.flyweight;

public class Building {
    private int x;
    private int y;
    private BuildingType type;

    public Building(int x, int y, BuildingType type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }
    public void info () {
        type.info();
    }
}
