package structure.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Map {
    private List<Building> buildings = new ArrayList<>();

    public void build(int x, int y, String name, String color, String otherTreeData) {
        BuildingType type = BuildingFactory.getBuildingType(name, color, otherTreeData);
        Building building = new Building(x, y, type);
        buildings.add(building);
    }
    public void showInfo() {
        for (Building building : buildings) {
            building.info();
        }
    }
}
