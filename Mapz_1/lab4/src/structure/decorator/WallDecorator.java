package structure.decorator;

abstract public class WallDecorator implements Wall{
    private Wall wall;

    public WallDecorator(Wall wall) {
        this.wall = wall;
    }

    @Override
    public void info() {
        wall.info();
    }
}

class DecoratorSpear extends WallDecorator{

    public DecoratorSpear(Wall wall) {
        super(wall);
    }

    @Override
    public void info() {
        super.info();
        addSpear();
    }
    void addSpear () {
        System.out.printf(" with spear ");
    }
}
class DecoratorCannon extends WallDecorator{
    public DecoratorCannon(Wall wall) {
        super(wall);
    }

    @Override
    public void info() {
        super.info();
        addCannon();
    }
    void addCannon () {
        System.out.printf(" with cannon ");
    }
}
class DecoratorResin extends WallDecorator{
    public DecoratorResin(Wall wall) {
        super(wall);
    }

    @Override
    public void info() {
        super.info();
        addResin();
    }
    void addResin () {
        System.out.printf(" with resin ");
    }
}