package structure.facade;

public class Facade {
    public static void main(String[] args) {
        Region region = new Region();
        region.CollectFromMine();
        region.CollectFromShop();
        region.CollectFromWall();
    }
}
