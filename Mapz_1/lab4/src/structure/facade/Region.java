package structure.facade;
public class Region {
    private int income = 0;
    private Building mine = new Mine();
    private Building shop = new Shop();
    private Building wall = new Wall();
    public int CollectFromMine() {
        mine.operate();
        return mine.giveMoney();
    }
    public int CollectFromWall() {
        wall.operate();
        return wall.giveMoney();
    }
    public int CollectFromShop() {
        shop.operate();
        return shop.giveMoney();
    }
    }

abstract class Building {
    private String id;
    protected int strenght;
    protected String wallType;
    protected int income;

    abstract void getInfo();

    public int giveMoney () {
        System.out.println("Income go into treasury");
        return income;
    }
    public void grove() {
        System.out.println("growing");
        income++;
    }
    public void operate() {
        System.out.println("Operate whith building");
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
class Mine extends Building {
    public Mine(){
        income = 30;
        strenght = 100;
        wallType = "wood";
    }
    @Override
    void getInfo() {
        System.out.println("Шахта");
    }
    @Override
    public void operate() {
        System.out.println("Operate whith mine");
    }
}
class Wall extends Building {
    public Wall(){
        income = -10;
        strenght = 1000;
        wallType = "stone";
    }
    @Override
    void getInfo() {
        System.out.println("Стіна");
    }
    @Override
    public void operate() {
        System.out.println("Operate whith wall");
    }
}
class Shop extends Building {
    public Shop(){
        income = 10;
        strenght = 200;
        wallType = "wood";
    }
    @Override
    void getInfo() {
        System.out.println("Магазин");
    }
    @Override
    public void operate() {
        System.out.println("Operate whith shop");
    }
}
