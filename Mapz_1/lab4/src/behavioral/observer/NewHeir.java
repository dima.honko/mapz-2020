package behavioral.observer;

public class NewHeir implements EventListener {

    @Override
    public void update(String name) {
        System.out.println("Новий спадкоємець" + name);
    }
}
