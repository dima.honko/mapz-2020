package behavioral.observer;

public interface EventListener {
    void update(String name);
}
