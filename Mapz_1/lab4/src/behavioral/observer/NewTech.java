package behavioral.observer;

public class NewTech implements EventListener {
    @Override
    public void update(String  name) {
        System.out.println("Нова технологія " + name);
    }
}
