package behavioral.comand;



public interface Command {
    void execute();
}

class Float implements Command {
    @Override
    public void execute() {
        System.out.println("Даний юніт пересувається лише по воді");
    }
}

class Walk  implements Command {
    @Override
    public void execute() {
        System.out.println("Даний юніт пересувається лише по рівнині");
    }
}

class Hiking implements Command {
    @Override
    public void execute() {
        System.out.println("Може пересуватися як по горах так і по рівнинах");
    }
}

class Complex implements  Command {
    String name;
    String envierment;
    GameLogic reciver;

    public Complex(String name, String envierment, GameLogic reciver) {
        this.name = name;
        this.envierment = envierment;
        this.reciver = reciver;
    }

    @Override
    public void execute() {
        reciver.displayName(name);
        reciver.displayType(envierment);
    }
}