package behavioral.memento;

import java.util.ArrayList;

public interface Save {
    String getName();
    ArrayList<Player> getPlayers();
}
