package behavioral.memento;

import structure.flyweight.Building;

import java.util.ArrayList;

public class Player {
    private ArrayList<Building> buildings;
    private String name;

    public Player(ArrayList<Building> buildings, String name) {
        this.buildings = buildings;
        this.name = name;
    }

    public void add (Building building) {
        buildings.add(building);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
