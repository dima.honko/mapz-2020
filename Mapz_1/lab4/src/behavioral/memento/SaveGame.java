package behavioral.memento;

import java.util.ArrayList;

public class SaveGame implements Save {
    private ArrayList<Player> players;
    private String name;

    public SaveGame(ArrayList<Player> players, String name) {
        this.name = name;
        this.players = new ArrayList<Player>();
        for (Player player : players) {
            this.players.add(player);
        }
    }

    @Override
    public ArrayList<Player> getPlayers() {
        return players;
    }

    @Override
    public String getName() {
        return name;
    }
}
